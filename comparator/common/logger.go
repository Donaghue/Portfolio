package common

import (
	"os"
	"log"
	"time"
	"bytes"
)

func WriteLog(msg string, sysErr error)  {
	var f *os.File
	var err error
	fileExists, _ := Exists("error.log")
	if fileExists {
		f, err = os.OpenFile("error.log", os.O_APPEND | os.O_WRONLY, 0644)
		if err != nil {
			log.Printf("can not open error.log file")
			log.Fatal(err)
		}
	} else {
		var file, err = os.Create("error.log")
		if err != nil {
			log.Printf("can not create file")
			log.Fatal(err)
		}
		defer file.Close()
	}
	var buf bytes.Buffer
	if sysErr != nil {
		buf.WriteString(time.Now().Format(time.ANSIC))
		buf.WriteString("\t")
		buf.WriteString(msg)
		buf.WriteString("\n")
		buf.WriteString(sysErr.Error())
		buf.WriteString("\n\n")

		_, err = f.WriteString(buf.String())
	} else {
		buf.WriteString(time.Now().Format(time.ANSIC))
		buf.WriteString("\t")
		buf.WriteString(msg)
		buf.WriteString("\n\n")

		_, err = f.WriteString(buf.String())
	}
	if err != nil {
		log.Printf("can not write string to error.log file")
		log.Fatal(err)
	}
	f.Close()
}

func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil { return true, nil }
	if os.IsNotExist(err) { return false, nil }
	return true, err
}