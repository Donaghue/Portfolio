package common

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
)


var instance *sql.DB

func GetDB() *sql.DB {
	if instance == nil {
		conf, _ := Configs()
		db, err := sql.Open("mysql", conf.DBRelation)
		if err != nil {
			DBOpenFailed(err)
		} else {
			instance = db
		}
	}
	return instance
}