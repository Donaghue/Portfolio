package common

import (
	"encoding/json"
	"../models"
	"io/ioutil"
	//"os"
)

func Configs() (*models.Config, []MyError){
	file, err := ioutil.ReadFile("../config/confProduction.json")
	var myErr []MyError
	if err != nil {
		myErr = append(myErr, CorruptedFilePath(err))
	}

	config := new(models.Config)
	err = json.Unmarshal(file, &config)
	if err != nil {
		myErr = append(myErr, CorruptedConfiguration(err))
	}
	return config, myErr
}
