package common

import (
	"strings"
	"../models"
	"../models/sql"
	"log"
)

func Compare(
	session *sql.Session,
	inputData []*models.BenchmarkData,
	expectedPossibleAmount int,
	expectedCoincidence float64,
	ProviderId int) {
	log.Print("got in comparator")
	percentForCategory := 2.0
	queryParameter := 20.0
	badWords := []string{" для ", " в ", " и ", " или ", " у ", " к ", " (", ") ", " gb ", " + ", "+"}
	res := make([]*sql.SessionResult, 0)
	siteProds := DBReader(ProviderId)
	log.Printf("got site pods")
	for ni, nv := range inputData {
		res = append(res, &sql.SessionResult{
			Categories: nv.Categories,
			Prods: make([]*sql.SComparedProds, 0),
		})
		for ji, jv := range nv.Goods {
			res[ni].Prods = append(res[ni].Prods, &sql.SComparedProds{
				Prod: jv.Name,
				Price: jv.Price,
				TrueProd: new(sql.SProds),
				PossibleProds: make([]*sql.SProds, 0),
			})
			//truePercent := 0.0
			wordsP := removeDuplicates(strings.Fields(removeWords(jv.Name, badWords)))
			for _, kv := range siteProds {
				percentNow := 0.0
				wordsS := removeDuplicates(strings.Fields(removeWords(kv.Name, badWords)))
				sameCategory := false
				if kv.Rating != 0 {
					percentNow += float64(kv.Rating) / 50.0
				}
				for i, iv := range wordsS {
					for l, lv := range wordsP {
						if strings.ToLower(iv) == strings.ToLower(lv) {
							if !sameCategory && stringInSlice(kv.CategoryName, nv.Categories) {
								percentNow += percentForCategory
								sameCategory = true
							}
							percentNow += queryParameter/float64(i+l+int(queryParameter))
						}

					}
				}

				//for _, iv := range nv.Products {
				//	for _, jv := range keywords {
				//		if (strings.ToUpper(jv) == strings.ToUpper(iv)) {
				//			percentNow++
				//		}
				//	}
				//}

				if percentNow < expectedCoincidence {
					continue
				}

				if len(res[ni].Prods[ji].PossibleProds) < expectedPossibleAmount {
					res[ni].Prods[ji].PossibleProds = append(res[ni].Prods[ji].PossibleProds, kv)
				} else {
					for i, iv := range res[ni].Prods[ji].PossibleProds {
						if iv.Percent < percentNow{
							res[ni].Prods[ji].PossibleProds[i] = kv
							res[ni].Prods[ji].PossibleProds[i].Percent = percentNow
							break
						}
					}
				}

				if res[ni].Prods[ji].TrueProd.Percent < percentNow {
					res[ni].Prods[ji].TrueProd = kv
					res[ni].Prods[ji].TrueProd.Percent = percentNow
					//truePercent = percentNow
				}
			}
			//maxPercent := 0.0
			//for _, iv := range res[ni].Prods[ji].PossibleProds {
			//	 if maxPercent < iv.Percent {
			//	 	maxPercent = iv.Percent
			//		 res[ni].Prods[ji].TrueProd = iv
			//	 }
			//}
			session.Processed++
		}
		log.Print(session.Processed)
		refreshSessionData(res, session)
	}
	refreshSessionData(res, session)
}