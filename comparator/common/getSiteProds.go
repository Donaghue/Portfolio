package common

import (
	_ "github.com/go-sql-driver/mysql"
	sqlModels "../models/sql"
)

func DBReader(ProviderId int) []*sqlModels.SProds {
	db := GetDB()

	rows, err := db.Query("SELECT g.id, g.name, g.rating, c.`name` as `category_name` FROM `goods` g LEFT JOIN `category` c ON g.`category_id` = c.`id` WHERE g.rating > 0")
	var myErr []MyError
	if err != nil {
		myErr = append(myErr, DBSelectFailed(err))
	}
	defer rows.Close()

	goods := make([]*sqlModels.SProds, 0)
	for rows.Next() {
		prod := new(sqlModels.SProds)
		err := rows.Scan(&prod.Id, &prod.Name, &prod.Rating, &prod.CategoryName)
		if err != nil {
			myErr = append(myErr, DBScanFailed(err))
		}
		goods = append(goods, prod)
	}
	if err = rows.Err(); err != nil {
		myErr = append(myErr, DBScanFailed(err))
	}

	return goods
}
