package common

import "strings"

func stringInSlice(a string, list []string) bool {
	for _, iv := range list {
		for _, kv := range strings.Fields(a) {
			if strings.ToLower(strings.Fields(iv)[0]) == strings.ToLower(kv) {
				return true
			}
		}
	}
	return false
}

func removeDuplicates(elements []string) []string {
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] == true {
		} else {
			encountered[elements[v]] = true
			result = append(result, elements[v])
		}
	}
	return result
}

func removeWords(a string, list []string) string  {
	ret := strings.ToLower(a)
	for _, v := range list {
		ret = strings.Replace(ret, v, " ", -1)
	}
	return ret
}

