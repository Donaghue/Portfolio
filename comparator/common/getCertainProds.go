package common

import (
	"encoding/json"
	"../models"
	"../models/sql"
	"log"
	"strings"
)

func GetCategories (ProdsJSON []byte, request []byte) []*models.Categories{
	log.Printf("GettingComplete")
	var comparatorResult []*sql.SessionResult
	json.Unmarshal(ProdsJSON, &comparatorResult)
	var out []*models.Categories
	json.Unmarshal(request, &out)

	db := GetDB()

	rows, err := db.Query("SELECT * FROM `goods`")
	var myErr []MyError
	if err != nil {
		myErr = append(myErr, DBSelectFailed(err))
	}
	defer rows.Close()

	goods := make(map[int]*sql.Product)
	for rows.Next() {
		prod := new(sql.Product)
		err := rows.Scan(
			&prod.Id,
			&prod.Name,
			&prod.Price,
			&prod.Sku,
			&prod.Description,
			&prod.Category_id,
			&prod.Link,
			&prod.Currency_id,
			&prod.Meta_key,
			&prod.Descrition_short,
			&prod.Rating,
			&prod.Meta_description,
			&prod.Created_at,
			&prod.Updated_at,
			&prod.Status_id,
			&prod.Keywords,
			)
		if err != nil {
			myErr = append(myErr, DBScanFailed(err))
		}
		goods[prod.Id] = prod
	}
	if err = rows.Err(); err != nil {
		myErr = append(myErr, DBScanFailed(err))
	}

	imagesRows, err := db.Query("SELECT `product_id`, `link` FROM `goods_image`")
	if err != nil {
		myErr = append(myErr, DBSelectFailed(err))
	}
	defer imagesRows.Close()
	images := make(map[int][]string)
	for imagesRows.Next() {
		im := new(sql.Images)
		err := imagesRows.Scan(&im.ProductId, &im.Link)
		if err != nil {
			myErr = append(myErr, DBScanFailed(err))
		}
		images[im.ProductId] = append(images[im.ProductId], im.Link)
	}
	if err = imagesRows.Err(); err != nil {
		myErr = append(myErr, DBScanFailed(err))
	}

	propGoodsRows, err := db.Query("SELECT `product_id`, `property_id`, `value` FROM `property_goods`")
	if err != nil {
		myErr = append(myErr, DBSelectFailed(err))
	}
	defer propGoodsRows.Close()
	propGoods := make([]*sql.PropertyGoods, 0)
	for propGoodsRows.Next() {
		PG := new(sql.PropertyGoods)
		err := propGoodsRows.Scan(&PG.ProductId, &PG.PropertyId, &PG.Value)
		if err != nil {
			myErr = append(myErr, DBScanFailed(err))
		}
		propGoods = append(propGoods, PG)
	}
	if err = propGoodsRows.Err(); err != nil {
		myErr = append(myErr, DBScanFailed(err))
	}

	propDictRows, err := db.Query("SELECT `id`, `name` FROM `property_dictionary`")
	if err != nil {
		myErr = append(myErr, DBSelectFailed(err))
	}
	defer propDictRows.Close()
	propDict := make(map[int]string)
	for propDictRows.Next() {
		PD := new(sql.PropertyDictionary)
		err := propDictRows.Scan(&PD.Id, &PD.Name)
		if err != nil {
			myErr = append(myErr, DBScanFailed(err))
		}
		propDict[PD.Id] = PD.Name
	}
	if err = propDictRows.Err(); err != nil {
		myErr = append(myErr, DBScanFailed(err))
	}

	log.Printf("getting parameters - done")

	var allProds []*models.ResultData
	for i, iv := range comparatorResult {
		allProds = append(allProds, &models.ResultData{
			Categories: iv.Categories,
			Products: make([]*models.ComparedProduct, 0),
		})
		for j, jv := range iv.Prods {
			allProds[i].Products = append(allProds[i].Products, &models.ComparedProduct{
				Prod: jv.Prod,
				Price: jv.Price,
				TrueProd: new(sql.Product),
				PossibleProd: make([]*sql.Product, 0),
			})
			if jv.TrueProd.Id != 0 {
				allProds[i].Products[j].TrueProd = goods[jv.TrueProd.Id]
				allProds[i].Products[j].TrueProd.CategoryName = jv.TrueProd.CategoryName
				allProds[i].Products[j].TrueProd.Percent = jv.TrueProd.Percent
				for k, kv := range jv.PossibleProds {
					allProds[i].Products[j].PossibleProd = append(allProds[i].Products[j].PossibleProd, goods[kv.Id])
					allProds[i].Products[j].PossibleProd[k].CategoryName = kv.CategoryName
					allProds[i].Products[j].PossibleProd[k].Percent = kv.Percent

				}
			}
		}
	}

	for j, jv := range allProds {
		for k, kv := range jv.Products {
			if kv.TrueProd != nil {
				for l, lv := range kv.PossibleProd {
					allProds[j].Products[k].PossibleProd[l].Images = images[lv.Id]
				}
				allProds[j].Products[k].TrueProd.Properties = make(map[string]string)
				for i := range kv.PossibleProd {
					allProds[j].Products[k].PossibleProd[i].Properties = make(map[string]string)
				}
				for _, iv := range propGoods {
					if kv.TrueProd.Id == iv.ProductId {
						allProds[j].Products[k].TrueProd.Properties[propDict[iv.PropertyId]] = iv.Value
					}
					for l, lv := range kv.PossibleProd {
						if lv.Id == iv.ProductId {
							allProds[j].Products[k].PossibleProd[l].Properties[propDict[iv.PropertyId]] = iv.Value
						}
					}
				}
			}
		}
	}

	for j, jv := range out {
		for _, iv := range allProds {
			if len(jv.SubCategories) != 0 {
				for k, kv := range jv.SubCategories {
					brek := false
					for _, lv := range kv.PossibleCategories {
						for _, mv := range iv.Categories {
							for _, nv := range strings.Fields(mv) {
								if strings.ToLower(lv) == strings.ToLower(nv) {
									for _, fv := range iv.Products {
										out[j].SubCategories[k].Goods = append(out[j].SubCategories[k].Goods, fv)
									}
									brek = true
								}
							}
							if brek {break}
						}
						if brek {break}
					}
				}
			} else {
				brek := false
				for _, lv := range iv.Categories {
					for _, nv := range strings.Fields(lv) {
						for _, kv := range jv.PossibleCategories {
							if strings.ToLower(kv) == strings.ToLower(nv) {
								for _, fv := range iv.Products {
									out[j].Goods = append(out[j].Goods, fv)
								}
								brek = true
							}
						}
						if brek {break}
					}
					if brek {break}
				}
			}
		}
	}

	log.Printf("data complete-formating - done")
	return out
}
