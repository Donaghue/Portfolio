package common

import (
	_ "github.com/go-sql-driver/mysql"
	"../models"
	"encoding/json"
	sqlModels "../models/sql"
	"time"
)

func AddSession (bData []*models.BenchmarkData) (*sqlModels.Session, []MyError){
	db := GetDB()


	Session := new(sqlModels.Session)
	Session.Id = RandString()
	Session.Status = "working"
	Session.Total = GetTotal(bData)
	Session.Processed = 0
	Session.CreatedAt = time.Now().String()

	var myErr []MyError
	_, err := db.Exec("INSERT INTO `session` VALUES(?, now(), ?, ?, ?, '[]')",
		Session.Id, Session.Status, Session.Total, Session.Processed)
	if err != nil {
		myErr = append(myErr, DBInsertFailed(err))
	}

	return Session, myErr
}

func refreshSessionData (res []*sqlModels.SessionResult, session *sqlModels.Session) {
	db := GetDB()

	if session.Processed == session.Total{
		session.Status = "complete"
	}

	if len(res) == 0 {
		return
	}

	tmp, _ := json.Marshal(res)
	session.Result = string(tmp)

	var myErr []MyError
	_, err := db.Exec("UPDATE `session` SET `status` = ?, `processed` = ?, `result` = ? WHERE `id`=?",
		session.Status, session.Processed, session.Result, session.Id)
	if err != nil {
		myErr = append(myErr, DBUpdateFailed(err))
	}
}

func GetStatus (SessionId string) (*sqlModels.SessionStatus, []MyError) {
	db := GetDB()

	row, err := db.Query("SELECT `id`, `created_at`, `status`, `total`, `processed`" +
		" FROM `session` WHERE `id`=?", SessionId)
	var myErr []MyError
	if err != nil {
		myErr = append(myErr, DBSelectFailed(err))
	}
	defer row.Close()

	currentSessionStatus := new(sqlModels.SessionStatus)
	
	row.Next()
	err = row.Scan(&currentSessionStatus.Id, &currentSessionStatus.CreatedAt, &currentSessionStatus.Status,
		&currentSessionStatus.Total, &currentSessionStatus.Processed)
	if err != nil {
		myErr = append(myErr, DBScanFailed(err))
	}

	return currentSessionStatus, myErr
}

func GetComplete (SessionId string, catRequest string) (*sqlModels.Session, []MyError) {
	db := GetDB()

	row, err := db.Query("SELECT * FROM `session` WHERE `id`=?", SessionId)
	var myErr []MyError
	if err != nil {
		myErr = append(myErr, DBSelectFailed(err))
	}
	defer row.Close()

	res := new(sqlModels.Session)
	
	row.Next()
	err = row.Scan(&res.Id, &res.CreatedAt, &res.Status,
		&res.Total, &res.Processed, &res.Result)
	if err != nil {
		myErr = append(myErr, DBScanFailed(err))
	}

	out, _ := json.Marshal(GetCategories([]byte(res.Result), []byte(catRequest)))
	res.Result = string(out)

	return res, myErr
}