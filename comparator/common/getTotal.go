package common

import "../models"

func GetTotal(inputData []*models.BenchmarkData) int {
	var total int
	for _, iv := range inputData {
		for range iv.Goods {
			total++
		}
	}
	return total
}
