package common

import (
	"time"
	"log"
)

type MyError struct {
	When time.Time
	What string
	Code int
	SysErr error
}

func Error(err error) MyError {
	msg := "failed to read DB data using such session id while Scan"
	code := 400
	if err != nil {
		log.Printf(msg + "\n" + err.Error())
	} else {
		log.Printf(msg)
	}
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func DBScanFailed(err error) MyError {
	msg := "failed to read DB data using such session id while Scan"
	code := 400
	log.Printf(msg + "\n" + err.Error())
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func DBSelectFailed (err error) MyError {
	msg := "failed to read DB data using such session id while SELECT"
	code := 400
	log.Printf(msg + "\n" + err.Error())
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func CorruptedConfiguration(err error) MyError {
	msg := "failed to use config. Check confProduction.json and make changes according to confDefault.txt"
	code := 400
	log.Printf(msg + "\n" + err.Error())
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func CorruptedFilePath(err error) MyError {
	msg := "file path is incorrect"
	code := 400
	log.Printf(msg + "\n" + err.Error())
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func CorruptedCategories(err error) MyError {
	msg := "Corrupted categories object has been received"
	code := 400
	log.Printf(msg)
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func DBInsertFailed(err error) MyError {
	msg := "cannot insert new session into DB"
	code := 400
	log.Printf(msg + "\n" + err.Error())
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func DBUpdateFailed(err error) MyError {
	msg := "cannot update session"
	code := 400
	log.Printf(msg + "\n" + err.Error())
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func DBOpenFailed(err error) MyError {
	msg := "failed to open DB"
	code := 502
	log.Printf(msg + "\n" + err.Error())
	WriteLog(msg, err)
	return MyError{
		time.Now(),
		msg,
		code,
		err,
	}
}

func IncorretURI() MyError {
	msg := "corrupted URI have been sent to server"
	code := 400
	log.Printf(msg)
	WriteLog(msg, nil)
	return MyError{
		time.Now(),
		msg,
		code,
		nil,
	}
}