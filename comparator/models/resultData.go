package models

import "./sql"

type ResultData struct {
	Categories []string				`json:"categories"`
	Products []*ComparedProduct		`json:"products"`
}

type ComparedProduct struct {
	Prod string						`json:"prod"`
	Price float64					`json:"price"`
	TrueProd *sql.Product			`json:true_prod`
	PossibleProd []*sql.Product		`json:"possible_prod"`
}