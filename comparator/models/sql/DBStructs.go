package sql

type Product struct {
	Id               int					`json:"id"`
	Name             string					`json:"name"`
	Price            int					`json:"price"`
	Sku              string					`json:"sku"`
	Description      string					`json:"description"`
	Category_id      int					`json:"category_id"`
	Link             string					`json:"link"`
	Currency_id      int					`json:"currency_id"`
	Meta_key         string					`json:"meta_key"`
	Descrition_short string					`json:"description_short"`
	Rating           int					`json:"rating"`
	Meta_description string					`json:"meta_description"`
	Created_at       string					`json:"created_at"`
	Updated_at       string					`json:"updated_at"`
	Status_id        int					`json:"status_id"`
	Keywords         string					`json:"keywords"`
	CategoryName	 string					`json:"category_name"`
	Images           []string				`json:"images"`
	Percent          float64				`json:"percent"`
	Properties		 map[string]string		`json:"properties"`
}

type Images struct {
	ProductId int							`json:"product_id"`
	Link string								`json:"link"`
}

type PropertyGoods struct {
	ProductId int							`json:"product_id"`
	PropertyId int							`json:"property_id"`
	Value string							`json:"value"`
}

type PropertyDictionary struct {
	Id int									`json:"id"`
	Name string								`json:"name"`
}