package sql

type Session struct {
	Id string
	CreatedAt string
	Status string
	Total int
	Processed int
	Result string
}

type SessionStatus struct {
	Id string
	CreatedAt string
	Status string
	Total int
	Processed int
}

type SessionResult struct {
	Categories []string
	Prods []*SComparedProds
}

type SComparedProds struct {
	Prod string
	Price float64
	TrueProd *SProds
	PossibleProds []*SProds
}

type SProds struct {
	Id int
	Name string
	Percent float64
	Rating int
	CategoryName string
}