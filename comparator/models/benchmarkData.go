package models

type BenchmarkData struct {
	Categories	[]string	`json:"categories"`
	Goods 		[]*Good		`json:"goods"`
}

type Good struct {
	Name string				`json:"name"`
	Price float64			`json:"price"`
}
