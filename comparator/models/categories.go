package models

type Categories struct {
	Name string						`json:"name"`
	Alias string					`json:"alias"`
	SubCategories []SubCategories	`json:"sub_categories"`
	PossibleCategories []string 	`json:"possible_categories"`
	Goods []*ComparedProduct 		`json:"goods"`
}

type SubCategories struct {
	Name string					`json:"name"`
	Alias string				`json:"alias"`
	PossibleCategories []string	`json:"possible_categories"`
	Goods []*ComparedProduct	`json:"goods"`
}