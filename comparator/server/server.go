package main

import (
	_ "github.com/go-sql-driver/mysql"
	"net/http"
	"encoding/json"
	"../models"
	"../common"
	"log"
)

type UserRequest struct {
	Data []*models.BenchmarkData
	PossibleAmount int
	Coincidience float64
	Provider int
}

func invokeComparator(w http.ResponseWriter, r *http.Request) {
	var ur UserRequest
	decode := json.NewDecoder(r.Body)
	decode.Decode(&ur)

	if len(ur.Data) != 0{

		currentSession, err := common.AddSession(ur.Data)

		go common.Compare(currentSession, ur.Data, ur.PossibleAmount, ur.Coincidience, ur.Provider)

		if len(err) != 0 {
			out, _ := json.Marshal(err)

			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Write(out)
		} else {
			out, _ := json.Marshal(currentSession)

			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Write(out)
		}
	} else {
		out, _ := json.Marshal(common.IncorretURI())

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write(out)
	}
}

func invokeStatus (w http.ResponseWriter, r *http.Request)  {
	str, ok := r.URL.Query()["session_id"]

	if ok && len(str) == 1 {
		SessionId := str[0]

		res, err := common.GetStatus(SessionId)
		if len(err) != 0 {
			out, _ := json.Marshal(err)

			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Write(out)
		} else {
			out, _ := json.Marshal(res)

			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Write(out)
		}
	} else {
		out, _ := json.Marshal(common.IncorretURI())

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write(out)
	}
}

func invokeComplete (w http.ResponseWriter, r *http.Request)  {
	str, ok := r.URL.Query()["session_id"]
	cat, ok2 := r.URL.Query()["categories"]

	if ok && len(str) == 1 {
		SessionId := str[0]
		var reqCategories string
		if len(cat) != 0 && ok2 {
			reqCategories = cat[0]
		} else {
			out, _ := json.Marshal(common.CorruptedCategories(nil))

			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Write(out)
		}

		res, err := common.GetComplete(SessionId, reqCategories)
		if len(err) != 0 {
			out, _ := json.Marshal(err)

			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Write(out)
		} else {
			out, _ := json.Marshal(res)

			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Write(out)
		}
	} else {
		out, _ := json.Marshal(common.IncorretURI())

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write(out)
	}
}

func main() {
	defer main()
	log.Printf("Comparator-server started")
	http.HandleFunc("/complete", invokeComplete)
	http.HandleFunc("/comparator", invokeComparator)
	http.HandleFunc("/status", invokeStatus)
	if err := http.ListenAndServe(":4201", nil); err != nil {
		panic(err)
	}
}