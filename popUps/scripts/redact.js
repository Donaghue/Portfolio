/**
 * Created by Limerick on 04.02.2018.
 */

function redactCard(editing) {
    var titleText, contentText;
    titleText = $(editing).parentsUntil(".card").find(".title").html();
    contentText = $(editing).parentsUntil(".card-wrapper").find(".content").html();

    $("#card-title").val(titleText);
    $("#card-content").val(contentText);
    $("#myModal").show();
    $(".save").click(function () {
        $(editing).parentsUntil(".card-container").remove();
    })
}
