/**
 * Created by Limerick on 31.01.2018.
 */
"use strict";

function deleteCard(thisCard) {
    $(".save").click(function () {
        $(thisCard).parentsUntil(".card-container").remove();
    })
}

function createCard() {
    $("<div>", { class: "cwcm"}).appendTo(".card-container");
    $("<div>", {class: "c1"}).appendTo(".cwcm");
    $("<div>", {class: "ch"}).appendTo(".c1");
    $("<button>", {class: "d", "data-toggle": "modal", "data-target": "#myModal1", onclick: "deleteCard(this)"}).appendTo(".ch");
    $("<i>", {class: "fas fa-times"}).appendTo(".d");
    $("<button>", {class: "r", "data-toggle": "modal", "data-target": "#myModal", onclick: "redactCard(this)"}).appendTo(".ch");
    $("<i>", {class: "fas fa-pencil-alt"}).appendTo(".r");
    $("<p>", {class: "t"}).appendTo(".ch")
    $(".t").append($("#card-title").val()).attr("class", "title");
    $("<div>", {class: "cc"}).appendTo(".c1");
    $("<p>", {class: "c2"}).appendTo(".cc");
    $(".c2").append($("#card-content").val()).attr("class", "content");

    $(".cwcm").attr("class", "card-wrapper col-md-2");
    $(".c1").attr("class", "card");
    $(".d").attr("class", "btn btn-danger delete");
    $(".r").attr("class", "btn btn-primary redact");
    $(".cc").attr("class", "card-content");
    $(".ch").attr("class", "card-header");
}
