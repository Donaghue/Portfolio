/**
 * Created by Limerick on 02.02.2018.
 */
function searchCard() {
    // Declare variables
    var input, card, title, p, i;
    input = $(".search-bar").val().toUpperCase();
    p = $(".title");
    card = $(".card-wrapper");

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < p.length; i++) {
        if (p[i].innerHTML.toUpperCase().indexOf(input) > -1) {
            card[i].style.display = "";
        } else {
            card[i].style.display = "none";
        }
    }
}
